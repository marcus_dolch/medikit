package dagger.de.medikit;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dagger.de.medikit.dao.Drug;

/**
 * Created by marcus on 19.08.16.
 */
public class CustomDrugAdapter extends ArrayAdapter {

    public CustomDrugAdapter(Context context, ArrayList<Drug> drugs) {
        super(context, 0, drugs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get data item from this position
        Drug drug = (Drug) getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.drug_list_widgets, parent, false);
        }

        // Lookup view for data population
        TextView txtName = (TextView) convertView.findViewById(R.id.txtListName);
        TextView txtId = (TextView) convertView.findViewById(R.id.txtListId);

        txtName.setText(drug.getName());
        txtId.setText(String.valueOf(drug.getId()));

        Log.d("CUSTOM_ADAPTER", drug.getId().toString());

        return convertView;
    }
}
