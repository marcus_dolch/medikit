package dagger.de.medikit;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import dagger.de.medikit.dao.Drug;
import dagger.de.medikit.dao.DrugDao;
import dagger.de.medikit.dao.DrugDaoImpl;

public class DailyPlanActivity extends AppCompatActivity {

    private TableLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_plan);

        DrugDaoImpl dao = new DrugDaoImpl();
        ArrayList<Drug> drugs = new ArrayList<>(dao.getDailyPlanDrugs());

        layout = (TableLayout) findViewById(R.id.tableLayout);
        setTableData(drugs);
    }

    private void setTableData(ArrayList<Drug> drugs) {

        boolean isBackgroundColored = false;

        for (Drug drug : drugs) {

            TableRow tr = new TableRow(this);

            // Colors every other row cyan.
            if (isBackgroundColored) {
                tr.setBackgroundColor(Color.CYAN);
                isBackgroundColored = false;
            }
            else {
                isBackgroundColored = true;
            } //

            TextView txtView = new TextView(this);
            txtView.setText(drug.getName());
            tr.addView(txtView);

            TextView txtMorning = new TextView(this);
            txtMorning.setText("" + drug.getDoseMorning());
            txtMorning.setGravity(Gravity.CENTER);
            tr.addView(txtMorning);

            TextView txtNoon = new TextView(this);
            txtNoon.setText("" + drug.getDoseNoon());
            txtNoon.setGravity(Gravity.CENTER);
            tr.addView(txtNoon);

            TextView txtEvening = new TextView(this);
            txtEvening.setText("" + drug.getDoseEvening());
            txtEvening.setGravity(Gravity.CENTER);
            tr.addView(txtEvening);

            TextView txtNight = new TextView(this);
            txtNight.setText("" + drug.getDoseNight());
            txtNight.setGravity(Gravity.CENTER);
            tr.addView(txtNight);

            layout.addView(tr);
        }
    }
}
