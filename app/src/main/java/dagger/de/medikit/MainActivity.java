package dagger.de.medikit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;

import java.util.ArrayList;
import java.util.List;

import dagger.de.medikit.dao.Drug;
import dagger.de.medikit.dao.DrugDaoImpl;

public class MainActivity extends AppCompatActivity {

    private ListView listDrugs;

    public final static String EXTRA_MESSAGE = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        ActiveAndroid.initialize(this);
        // Drug drugMigration = new Drug();

        listDrugs = (ListView) findViewById(R.id.listDrugs);

        ArrayList<Drug> drugs = setListData();
        listDrugs.setAdapter(setCustomAdapter(drugs));

        listDrugs.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView txtId = (TextView) view.findViewById(R.id.txtListId);
                Log.d("ATTENTION", view.toString());

                // Starting SaveDrug Activity with ID as extra message.
                Intent intent = new Intent(getApplicationContext(), SaveDrug.class);
                String message = String.valueOf(txtId.getText().toString());
                intent.putExtra("id", message);
                Log.d("ID", message);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();

        ArrayList<Drug> drugs = setListData();
        listDrugs.setAdapter(setCustomAdapter(drugs));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                Log.d("BUTTON", "Pressed " + item.getItemId());
                Intent i = new Intent(this, SaveDrug.class);
                startActivity(i);
                return true;

            case R.id.menu_daily:
                Intent dailyIntent = new Intent(this, DailyPlanActivity.class);
                startActivity(dailyIntent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private ArrayList<Drug> setListData() {

        DrugDaoImpl dao = new DrugDaoImpl();

        return new ArrayList<>(dao.getAllDrugs());
    }

    private CustomDrugAdapter setCustomAdapter(ArrayList<Drug> drugs) {

        CustomDrugAdapter adapter = new CustomDrugAdapter(this, drugs);
        adapter.setNotifyOnChange(true);

        return adapter;
    }

}
