package dagger.de.medikit;

import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class OptionsActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "MediKitPrefs";

    private CheckBox checkNotifications;
    private EditText txtNotificationMorning;
    private EditText txtNotificationNoon;
    private EditText txtNotificationEvening;
    private EditText txtNotificationNight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        checkNotifications = (CheckBox) findViewById(R.id.checkNotifications);
        txtNotificationMorning = (EditText) findViewById(R.id.txtNotiMorning);
        txtNotificationNoon = (EditText) findViewById(R.id.txtNotiNoon);
        txtNotificationEvening = (EditText) findViewById(R.id.txtNotiEvening);
        txtNotificationNight = (EditText) findViewById(R.id.txtNotiNight);

        /*
        SharedPreferences allows to save and retrieve persistetn key-value
        pairs of primitive data types.
         */
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean notificationsOn = settings.getBoolean("notifications", false);
        String notMorning = settings.getString("notification_morning", "");
        String notNoon = settings.getString("notification_noon", "");
        String notEvening = settings.getString("notification_evening", "");
        String notNight = settings.getString("notification_night", "");

        checkNotifications.setChecked(notificationsOn);

        if (!notMorning.isEmpty())
            txtNotificationMorning.setText(notMorning);
        if (!notNoon.isEmpty())
            txtNotificationNoon.setText(notNoon);
        if (!notEvening.isEmpty())
            txtNotificationEvening.setText(notEvening);
        if (!notNight.isEmpty())
            txtNotificationNight.setText(notNight);
    }

    /*
    Saves the user preferences in the Shared Preferences file.
     */
    public void saveOptions(View view) {

        boolean isNotificationsActivated = checkNotifications.isChecked();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean("notifications", isNotificationsActivated);
        editor.putString("notification_morning", txtNotificationMorning.getText().toString());
        editor.putString("notification_noon", txtNotificationNoon.getText().toString());
        editor.putString("notification_evening", txtNotificationEvening.getText().toString());
        editor.putString("notification_night", txtNotificationNight.getText().toString());

        editor.commit();
    }
}
