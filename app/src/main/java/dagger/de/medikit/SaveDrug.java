package dagger.de.medikit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import dagger.de.medikit.dao.Drug;
import dagger.de.medikit.dao.DrugDaoImpl;

public class SaveDrug extends AppCompatActivity {

    private EditText txtId;
    private EditText txtEan;
    private EditText txtName;
    private EditText txtAmount;
    private EditText txtCurrentAmount;
    private EditText txtSubstance;
    private EditText txtDoseMorning;
    private EditText txtDoseNoon;
    private EditText txtDoseEvening;
    private EditText txtDoseNight;

    private Button btnSave;
    private Button btnClear;

    private String TAG = "SAVE_DRUG_ACTIVITY";
    private String idFromIntent = "";

    private DrugDaoImpl dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_drug);

        dao = new DrugDaoImpl();

        Intent intent = getIntent();
        idFromIntent = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        txtId = (EditText) findViewById(R.id.editId);
        txtEan = (EditText) findViewById(R.id.txtEan);
        txtName = (EditText) findViewById(R.id.txtNameMed);
        txtAmount = (EditText) findViewById(R.id.txtAmount);
        txtCurrentAmount = (EditText) findViewById(R.id.txtCurrentAmount);
        txtSubstance = (EditText) findViewById(R.id.txtSubstance);
        txtDoseMorning = (EditText) findViewById(R.id.txtDoseMorning);
        txtDoseNoon = (EditText) findViewById(R.id.txtDoseNoon);
        txtDoseEvening = (EditText) findViewById(R.id.txtDoseEvening);
        txtDoseNight = (EditText) findViewById(R.id.txtDoseNight);

        txtId.setText(idFromIntent);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (idFromIntent == null) {
                    addDrug();
                }
                else {
                    updateDrug(getFormData());
                    Log.d(TAG, idFromIntent);
                }
            }
        });

        btnClear = (Button) findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearForm((ViewGroup) findViewById(R.id.parentLayout));
            }
        });

        // Checks if form is activated through list activity.
        // If so, the form is going to be filled with the specific data.
        if (idFromIntent != null) {

            loadDrug(Integer.parseInt(idFromIntent));
        }
    }

    private void loadDrug(int id) {

        // DrugDaoImpl dao = new DrugDaoImpl();
        Drug drug = dao.getDrugById(id);

        setFormData(drug);
    }

    public void addDrug() {

        // DrugDaoImpl dao = new DrugDaoImpl();
        dao.addDrug(getFormData());
        Log.d(TAG, "Drug succesfully saved.");
        clearForm((ViewGroup) findViewById(R.id.parentLayout));
    }

    public void updateDrug(Drug drug) {

        // DrugDaoImpl dao = new DrugDaoImpl();
        dao.updateDrug(Integer.parseInt(txtId.getText().toString()), drug);
    }

    public Drug getFormData() {

        Drug drug = new Drug();

        try {
            drug.setName(txtName.getText().toString());
            drug.setEanCode(txtEan.getText().toString());
            drug.setAmount(Float.valueOf(txtAmount.getText().toString()));
            drug.setCurrentAmount(Float.valueOf(txtCurrentAmount.getText().toString()));
            drug.setSubstance(txtSubstance.getText().toString());
            drug.setDoseMorning(Float.valueOf(txtDoseMorning.getText().toString()));
            drug.setDoseNoon(Float.valueOf(txtDoseNoon.getText().toString()));
            drug.setDoseEvening(Float.valueOf(txtDoseEvening.getText().toString()));
            drug.setDoseNight(Float.valueOf(txtDoseNight.getText().toString()));
            return drug;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }

    public void setFormData(Drug drug) {

        txtName.setText(drug.getName());
        txtEan.setText(drug.getEanCode());
        txtSubstance.setText(drug.getSubstance());
        txtAmount.setText(String.valueOf(drug.getAmount()));
        txtCurrentAmount.setText(String.valueOf(drug.getCurrentAmount()));
        txtDoseMorning.setText(String.valueOf(drug.getDoseMorning()));
        txtDoseEvening.setText(String.valueOf(drug.getDoseEvening()));
        txtDoseNight.setText(String.valueOf(drug.getDoseNight()));
        txtDoseNoon.setText(String.valueOf(drug.getDoseNoon()));
    }

    /*
        Loops through the layout file and searches for EditText fields.
        If an EditText is found, the field is going to be cleared.
        If another ViewGroup such as LinearLayout is found it recursively
        calls itself.
     */
    public void clearForm(ViewGroup view) {

        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);

            if (v instanceof EditText) {
                ((EditText) v).setText("");
            }
            else if (v instanceof ViewGroup) {

                this.clearForm((ViewGroup) v);
            }
        }
    }

    public void closeActivity(View view) {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                int id = Integer.valueOf(txtId.getText().toString());
                dao.deleteDrug(dao.getDrugById(id));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
