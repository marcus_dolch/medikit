package dagger.de.medikit;

import dagger.de.medikit.dao.Drug;
import dagger.de.medikit.dao.DrugDaoImpl;

/**
 * Created by marcus on 19.08.16.
 */
public class TestData {

    public TestData() {

        Drug d1 = new Drug();
        d1.setName("Belok Zok (MetoHexal Succ) 95mg");
        d1.setEanCode("");
        d1.setAmount(100.0f);
        d1.setSubstance("Metoprololsuccinat");
        d1.setDoseMorning(1.0f);
        d1.setDoseNoon(0.0f);
        d1.setDoseEvening(0.0f);
        d1.setDoseNight(0.0f);

        Drug d2 = new Drug();
        d2.setName("Allopurinol (Allobeta) 100mg");
        d2.setEanCode("");
        d2.setAmount(100.0f);
        d2.setSubstance("Allopurinol");
        d2.setDoseMorning(1.0f);
        d2.setDoseNoon(0.0f);
        d2.setDoseEvening(0.0f);
        d2.setDoseNight(0.0f);

        /*
        Drug d3 = new Drug();
        d1.setName("Urbason 4mg");
        d1.setEanCode("");
        d1.setAmount(100.0f);
        d1.setSubstance("Methylprednisolon");
        d1.setDoseMorning(1.0f);
        d1.setDoseNoon(0.0f);
        d1.setDoseEvening(0.0f);
        d1.setDoseNight(0.0f);*/

        DrugDaoImpl dao = new DrugDaoImpl();
        d1.save();
        // d2.save();
    }
}
