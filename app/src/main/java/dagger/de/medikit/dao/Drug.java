package dagger.de.medikit.dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Drug")
public class Drug extends Model {

    @Column
    private String eanCode;
    @Column
    private String name;
    @Column
    private String substance;
    @Column
    private float amount;
    @Column
    private float currentAmount;
    @Column
    private float doseMorning;
    @Column
    private float doseNoon;
    @Column
    private float doseEvening;
    @Column
    private float doseNight;

    public Drug() {}

    public Drug(String eanCode, String name, String substance, float amount, float doseMorning, float doseNoon, float doseEvening, float doseNight) {
        this.eanCode = eanCode;
        this.name = name;
        this.substance = substance;
        this.amount = amount;
        this.doseMorning = doseMorning;
        this.doseNoon = doseNoon;
        this.doseEvening = doseEvening;
        this.doseNight = doseNight;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubstance() {
        return substance;
    }

    public void setSubstance(String substance) {
        this.substance = substance;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getDoseMorning() {
        return doseMorning;
    }

    public void setDoseMorning(float doseMorning) {
        this.doseMorning = doseMorning;
    }

    public float getDoseNoon() {
        return doseNoon;
    }

    public void setDoseNoon(float doseNoon) {
        this.doseNoon = doseNoon;
    }

    public float getDoseEvening() {
        return doseEvening;
    }

    public void setDoseEvening(float doseEvening) {
        this.doseEvening = doseEvening;
    }

    public float getDoseNight() {
        return doseNight;
    }

    public void setDoseNight(float doseNight) {
        this.doseNight = doseNight;
    }

    public float getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(float currentAmount) {
        this.currentAmount = currentAmount;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "id='" + getId() + '\'' +
                "name='" + name + '\'' +
                '}';
    }
}
