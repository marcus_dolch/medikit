package dagger.de.medikit.dao;

import java.util.List;

/**
 * Created by marcus on 19.08.16.
 */
public interface DrugDao {

    public List<Drug> getAllDrugs();
    public Drug getDrugById(int id);
    public List<Drug> getDailyPlanDrugs();
    public void addDrug(Drug drug);
    public void updateDrug(int id, Drug updatedDrug);
    public void deleteDrug(Drug drug);

}
