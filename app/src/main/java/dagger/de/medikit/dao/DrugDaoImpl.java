package dagger.de.medikit.dao;

import android.content.Intent;

import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by marcus on 19.08.16.
 */
public class DrugDaoImpl implements DrugDao {

    public DrugDaoImpl() {}

    @Override
    public List<Drug> getAllDrugs() {
        return new Select()
                .all()
                .from(Drug.class)
                .orderBy("name ASC")
                .execute();
    }

    @Override
    public Drug getDrugById(int id) {
        return new Select()
                .from(Drug.class)
                .where("id = ?", id)
                .executeSingle();
    }

    @Override
    public List<Drug> getDailyPlanDrugs() {
        return new Select()
                .from(Drug.class)
                // Can't that be solved better?
                .where("doseMorning > 0 or doseNoon > 0 or doseEvening > 0 or doseNight > 0")
                .orderBy("name ASC")
                .execute();
    }

    @Override
    public void addDrug(Drug drug) {
        drug.save();
    }

    @Override
    public void updateDrug(int id, Drug updatedDrug) {

        Drug newDrug = getDrugById(id);
        newDrug.setName(updatedDrug.getName());
        newDrug.setEanCode(updatedDrug.getEanCode());
        newDrug.setAmount(updatedDrug.getAmount());
        newDrug.setCurrentAmount(updatedDrug.getCurrentAmount());
        newDrug.setSubstance(updatedDrug.getSubstance());
        newDrug.setDoseMorning(updatedDrug.getDoseMorning());
        newDrug.setDoseNoon(updatedDrug.getDoseNoon());
        newDrug.setDoseEvening(updatedDrug.getDoseEvening());
        newDrug.setDoseNight(updatedDrug.getDoseNight());
        newDrug.save();
    }

    @Override
    public void deleteDrug(Drug drug) {
        drug.delete();
    }
}
